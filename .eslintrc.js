module.exports = {
	root: true,
	env: {
		node: true
	},
	extends: ["plugin:vue/recommended"],
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? "error" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
		"indent": [2, "tab"]
	},
	// redundant rules:
	// max-attributes-per-line
	// attributes-order?
	// vue/singleline-html-element-content-newline
	// vue/require-default-prop
	parserOptions: {
		parser: "babel-eslint"
	},
	plugins: ["@vue"]
};
