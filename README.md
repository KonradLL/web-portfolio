# Web Portfolio
- [Live Demo](https://konradll.gitlab.io/web-portfolio)
This is source code for my personal web portfolio

## Setup
``` sh
#first make sure all the necessary node_modules are installed with 
npm install
#then you can run the project in with
npm run serve
```

### Project structure
the src folder contains all the source code...

# ToDo (outdated)
This section is about what needs to be done for each portfolio section and what the idea is for each of them.

## Cover
The cover/landing section will be a static image that will be split into 3 horizontal sections. Each section will have a text and beneath it a pictaure of code thats been darkened, like [here](http://ejosue.com/). Each section picture will be of code a different IDE, and ontop of text with verb + 'code'. the following combo's are planned: 
* vscode && 'robust code'
* sublime && 'fast code'
* atom && 'clean code'

## About
This section is split up into columns, each giving its own information:
- Me, picture and a short about me
- my personal interests
- music Im currently into

## skills
- my code phylosopy: reliable, fast & clean

## Timeline
Have a timeline that shows your career

## Projects
Have a small section that shows the projects that you worked on, something like englishaday.com user-stories

## Contact
Contact
